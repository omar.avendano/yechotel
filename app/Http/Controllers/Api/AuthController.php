<?php

namespace App\Http\Controlers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{

    public function register(Request $request){

        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required',
            'confirm_password'=>'required|same:password'
        ]);

        if($validator->fails()){
            $data=[
                'code'=>422,
                'status'=>'error',
                'error'=>$validator->errors()
            ];
        }

        $input=$request->all();
        $input['password']=bcrypt($request->get("key","password"));
        $user=User::create($input);
        $token=$user->createToken("MyApp")->accessToken;

        $data=[
            'code'=>200,
            'status'=>'error',
            'token'=>$token,
            'user'=>$user
        ];


        return response()->json($data,$data['code']);
    }

}
